package polynomoperations;

import java.util.ArrayList;

import mon.Monom;
import polynomoperations.Enums.Operations;

public class PolynomOperations {

	Polynom resultPolynom = new Polynom();
	int finalPolynomDegree;


	public String sum(ArrayList<Polynom> polynoms) {

		ArrayList<Monom> localMonoms = new ArrayList<Monom>();

		ArrayList<Monom> polynom1Monoms = polynoms.get(0).monoms;
		ArrayList<Monom> polynom2Monoms = polynoms.get(1).monoms;

		for (Monom monom : polynom1Monoms) {
			boolean monomAdded = false;
			for (Monom monom2 : polynom2Monoms) {

				if (monom.Exp() == monom2.Exp()) {
					int temp = monom.Coef() + monom2.Coef();
					Monom tempMonom = new Monom(temp, monom.Exp());
					localMonoms.add(tempMonom);
					temp = 0;
					monomAdded = true;
					break;
				}
			}
			if(!monomAdded)
				localMonoms.add(monom);
		}
		resultPolynom.monoms = localMonoms;

		String resultString = "";
		for (Monom monom : resultPolynom.monoms) {
			resultString += "+" + Integer.toString(monom.Coef()) + "X^" + Integer.toString(monom.Exp());
		}
		return resultString;
	}

	public String substract(ArrayList<Polynom> polynoms) {

		ArrayList<Monom> localMonoms = new ArrayList<Monom>();

		ArrayList<Monom> polynom1Monoms = polynoms.get(0).monoms;
		ArrayList<Monom> polynom2Monoms = polynoms.get(1).monoms;

		for (Monom monom : polynom1Monoms) {
			boolean monomAdded = false;
			for (Monom monom2 : polynom2Monoms) {

				if (monom.Exp() == monom2.Exp()) {
					int temp = monom.Coef() - monom2.Coef();
					Monom tempMonom = new Monom(temp, monom.Exp());
					localMonoms.add(tempMonom);
					temp = 0;
					monomAdded = true;
					break;
				}
			}
			if(!monomAdded)
				localMonoms.add(monom);
		}
		resultPolynom.monoms = localMonoms;

		String resultString = "";
		for (Monom monom : resultPolynom.monoms) {
			resultString += "+" + Integer.toString(monom.Coef()) + "X^" + Integer.toString(monom.Exp());
		}
		return resultString;
	}

	public String multiply(ArrayList<Polynom> polynoms) {

		ArrayList<Monom> localMonoms = new ArrayList<Monom>();

		ArrayList<Monom> polynom1Monoms = polynoms.get(0).monoms;
		ArrayList<Monom> polynom2Monoms = polynoms.get(1).monoms;

		for (Monom monom : polynom1Monoms) {
			boolean monomAdded = false;
			for (Monom monom2 : polynom2Monoms) {

				if (monom.Exp() == monom2.Exp()) {
					int temp = monom.Coef() * monom2.Coef();
					int tempExp = monom.Exp() + monom2.Exp();
					Monom tempMonom = new Monom(temp, tempExp);
					localMonoms.add(tempMonom);
					temp = 1;
					tempExp = 1;
					monomAdded = true;
					break;
				}
			}
			if(!monomAdded)
				localMonoms.add(monom);
		}
		resultPolynom.monoms = localMonoms;

		String resultString = "";
		for (Monom monom : resultPolynom.monoms) {
			resultString += "+" + Integer.toString(monom.Coef()) + "X^" + Integer.toString(monom.Exp());
		}
		return resultString;
	}

	public String devide(ArrayList<Polynom> polynoms) {
		ArrayList<Monom> localMonoms = new ArrayList<Monom>();

		ArrayList<Monom> polynom1Monoms = polynoms.get(0).monoms;
		ArrayList<Monom> polynom2Monoms = polynoms.get(1).monoms;

		for (Monom monom : polynom1Monoms) {
			boolean monomAdded = false;
			for (Monom monom2 : polynom2Monoms) {

				if (monom.Exp() == monom2.Exp()) {
					int temp = monom.Coef() / monom2.Coef();
					int tempExp = monom.Exp() - monom2.Exp();
					Monom tempMonom = new Monom(temp, tempExp);
					localMonoms.add(tempMonom);
					temp = 1;
					tempExp = 1;
					monomAdded = true;
					break;
				}
			}
			if(!monomAdded)
				localMonoms.add(monom);
		}
		resultPolynom.monoms = localMonoms;

		String resultString = "";
		for (Monom monom : resultPolynom.monoms) {
			resultString += "+" + Integer.toString(monom.Coef()) + "X^" + Integer.toString(monom.Exp());
		}
		return resultString;
	}
	
	//
	
	public String deriv(ArrayList<Polynom> polynoms) {
		ArrayList<Monom> polynom1Monoms = polynoms.get(0).monoms;
		
		String resultString = "";
		for (Monom monom : polynom1Monoms) {
			
			if(monom.Exp() !=0)
			{
				resultString += "+" + Integer.toString(monom.Coef()*monom.Exp())+ "X^" + Integer.toString(monom.Exp()-1);

			}
		}
				
		return resultString;
	}
	
	//
	
	public String integr(ArrayList<Polynom> polynoms) {
      ArrayList<Monom> polynom1Monoms = polynoms.get(0).monoms;
		String resultString = "";
       
			for(Monom monom : polynom1Monoms) {
			if(monom.Exp() !=0 )
			{
				resultString += "+" +( Integer.toString(monom.Coef())+"X^"+Integer.toString(monom.Exp()+1)+  "/" + Integer.toString(monom.Exp()+1));
			
			}
			else
			{
				resultString += "+" +(Integer.toString(monom.Exp()+1)+ "X^"+ Integer.toString(monom.Coef()));
			}
		}
		return resultString;
	}
 
}
