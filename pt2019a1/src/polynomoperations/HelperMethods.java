package polynomoperations;

//import java.awt.List;
import java.util.ArrayList;

import mon.Monom;
import polynomoperations.Enums.Operations;

public class HelperMethods {

	public ArrayList<Polynom> cleanPolynoms(String pol1, String pol2) {
		ArrayList<Polynom> polynoms = new ArrayList<Polynom>();
		String[] cleanedPolynoms = new String[2];
		pol1 = pol1.replace(" ", "");
		pol2 = pol2.replace(" ", "");

		cleanedPolynoms[0] = pol1;
		cleanedPolynoms[1] = pol2;

		for (String polynom : cleanedPolynoms) {
			polynoms.add(this.anotherDecomposition(polynom));

		}
		return polynoms;

	}

	public ArrayList<Polynom> cleanPolynoms1(String pol1) {
		ArrayList<Polynom> polynoms = new ArrayList<Polynom>();
		String[] cleanedPolynoms = new String[1];
		pol1 = pol1.replace(" ", "");
	
		cleanedPolynoms[0] = pol1;
		for (String polynom : cleanedPolynoms) {
			polynoms.add(this.anotherDecomposition(polynom));

		}
		return polynoms;

	}
	
	
	
	
	private Polynom anotherDecomposition(String polynomForDecomposion) {
		int coefVal = 0;
		int coefDegree = 0;
		ArrayList<String> finalPolynomMonoms = new ArrayList<String>();
		ArrayList<Monom> monoms = new ArrayList<Monom>();

		String[] polynomMonoms = polynomForDecomposion.split("\\+");
		for (String item : polynomMonoms) {
			int minusIndex = item.indexOf("-");
			if(minusIndex != -1) {
				finalPolynomMonoms.add(item.substring(0, minusIndex));
				finalPolynomMonoms.add(item.substring(minusIndex));
			}
			else 
				finalPolynomMonoms.add(item);				
		} 
		
		
		int polynomDegree = finalPolynomMonoms.toArray().length;

		for (String item : finalPolynomMonoms) {
			int index = item.indexOf("x");
			if (index != -1 && index == 0) {
				coefVal = 1;
				coefDegree = getVariableDegree(item);
			} else if (index != -1 && index != 0) {
				coefVal = Integer.parseInt(item.substring(0, index));
				coefDegree = getVariableDegree(item);
			} else
				coefVal = Integer.parseInt(item);
			
			Monom tempMonom = new Monom(coefVal, coefDegree);
					
			monoms.add(tempMonom);
			coefVal = 0;
			coefDegree = 0;
		}
		Polynom polynom = new Polynom();
		polynom.monoms = monoms;
		polynom.polynomDegree = polynomDegree;
		return polynom;
	};

	private int getVariableDegree(String monom) {
		String temp;
		int index = monom.indexOf('^');
		if (index != -1)
			{
			temp = monom.substring(index+1);
			return Integer.parseInt(temp);
			}
			
		else
			return 1;
	}
}
