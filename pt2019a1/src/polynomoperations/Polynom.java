package polynomoperations;

import java.util.ArrayList;

import mon.Monom;

public class Polynom {
	public int VariableCoeficient;
    public int SingleCoeficient;
    public Enums.Operations operation;
    public ArrayList<Monom> monoms;
    public int polynomDegree;

}
