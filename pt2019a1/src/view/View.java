package view;

//import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
//import javax.swing.JPanel;
import javax.swing.JTextField;
//import javax.swing.SwingUtilities;

import polynomoperations.HelperMethods;
import polynomoperations.*;

public class View extends JFrame {
	
	
	private JTextField primPolinField;
	private JTextField secPolinField;
		
	public JFrame frame;
	private JButton sum;
	private JButton substract;
	private JButton multiply;
	private JButton devide;	
	private JButton deriv;	
	private JButton integr;	
	
	public View() {
		createFrame();
		createView();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(700,600);
		setLocationRelativeTo(null);
		setResizable(false);
	}
	
	private void createFrame()
	{
		frame = new JFrame();
        frame.setBounds(100, 100, 730, 489);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        
	}

	private void createView() {		
		
		JLabel labelP1= new JLabel ("Polinom1");
		labelP1.setBounds(65, 31, 150, 20);
		frame.getContentPane().add(labelP1);
		
		primPolinField=new JTextField();
		primPolinField.setBounds(128, 31, 150, 20);
		frame.getContentPane().add(primPolinField);
		
		JLabel labelP2= new JLabel ("Polinom2");
		labelP2.setBounds(65, 70, 150, 20);
		frame.getContentPane().add(labelP2);

		secPolinField =new JTextField();
		secPolinField.setBounds(128, 70, 150, 20);
		frame.getContentPane().add(secPolinField);
	    		
		JLabel labelRez= new JLabel("Result:");
		labelRez.setBounds(350, 50, 150, 20);
		frame.getContentPane().add(labelRez);
		
		JLabel labelActualResult= new JLabel();
		labelActualResult.setBounds(450, 50, 150, 20);
		frame.getContentPane().add(labelActualResult);
	
		sum= new JButton("Adunare");		
		sum.setBounds(65, 95, 150, 20);
		frame.getContentPane().add(sum);
		
		substract= new JButton("Scadere");
		substract.setBounds(65, 120, 150, 20);
		frame.getContentPane().add(substract);
		
		multiply= new JButton("Inmultire");
		multiply.setBounds(65, 145, 150, 20);;
		frame.getContentPane().add(multiply);
		
		devide= new JButton("Impartire");
		devide.setBounds(65, 170, 150, 20);
		frame.getContentPane().add(devide);
		
		deriv= new JButton("Derivate");
		deriv.setBounds(65, 195, 150, 20);
		frame.getContentPane().add(deriv);
		
		integr= new JButton("Integrate");
		integr.setBounds(65, 220, 150, 20);
		frame.getContentPane().add(integr);
		
		
		sum.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) 
            {
            	ArrayList<Polynom> polynoms;
            	
                if(primPolinField.getText().isEmpty()||(secPolinField.getText().isEmpty()))
                    JOptionPane.showMessageDialog(null, "Please set both polynoms...");
                else       
                {
                	String firstPolynom = primPolinField.getText();
                	String secondPolynom = secPolinField.getText();
                	
                	polynoms = new HelperMethods().cleanPolynoms(firstPolynom, secondPolynom);
                	
                	PolynomOperations operationsAccess = new PolynomOperations();
                	labelActualResult.setText(operationsAccess.sum(polynoms));
                }
            }
        });
		
		substract.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) 
            {
            	ArrayList<Polynom> polynoms;
            	
                if(primPolinField.getText().isEmpty()||(secPolinField.getText().isEmpty()))
                    JOptionPane.showMessageDialog(null, "Please set both polynoms...");
                else       
                {
                	String firstPolynom = primPolinField.getText();
                	String secondPolynom = secPolinField.getText();
                	
                	polynoms = new HelperMethods().cleanPolynoms(firstPolynom, secondPolynom);
                	
                	PolynomOperations operationsAccess = new PolynomOperations();
                	labelActualResult.setText(operationsAccess.substract(polynoms));
                }
            }
        });
		
		
		multiply.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) 
            {
            	ArrayList<Polynom> polynoms;
            	
                if(primPolinField.getText().isEmpty()||(secPolinField.getText().isEmpty()))
                    JOptionPane.showMessageDialog(null, "Please set both polynoms...");
                else       
                {
                	String firstPolynom = primPolinField.getText();
                	String secondPolynom = secPolinField.getText();
                	
                	polynoms = new HelperMethods().cleanPolynoms(firstPolynom, secondPolynom);
                	
                	PolynomOperations operationsAccess = new PolynomOperations();
                	labelActualResult.setText(operationsAccess.multiply(polynoms));
                }
            }
        });
		
		devide.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) 
            {
            	ArrayList<Polynom> polynoms;
            	
                if(primPolinField.getText().isEmpty()||(secPolinField.getText().isEmpty()))
                    JOptionPane.showMessageDialog(null, "Please set both polynoms...");
                else       
                {
                	String firstPolynom = primPolinField.getText();
                	String secondPolynom = secPolinField.getText();
                	
                	polynoms = new HelperMethods().cleanPolynoms(firstPolynom, secondPolynom);
                	
                	PolynomOperations operationsAccess = new PolynomOperations();
                	labelActualResult.setText(operationsAccess.devide(polynoms));
                }
            }
        });
		
		
		deriv.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) 
            {
            	ArrayList<Polynom> polynoms;
            	
                if(primPolinField.getText().isEmpty())
                    JOptionPane.showMessageDialog(null, "Please set a polynoms...");
                else       
                {
                	String firstPolynom = primPolinField.getText();
                	//String secondPolynom = secPolinField.getText();
                	
                	polynoms = new HelperMethods().cleanPolynoms1(firstPolynom);
                	
                	PolynomOperations operationsAccess = new PolynomOperations();
                	labelActualResult.setText(operationsAccess.deriv(polynoms));
                }
            }
        });
		
		
		integr.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) 
            {
            	ArrayList<Polynom> polynoms;
            	
                if(primPolinField.getText().isEmpty())
                    JOptionPane.showMessageDialog(null, "Please set a polynoms...");
                else       
                {
                	String firstPolynom = primPolinField.getText();
                	//String secondPolynom = secPolinField.getText();
                	
                	polynoms = new HelperMethods().cleanPolynoms1(firstPolynom);
                	
                	PolynomOperations operationsAccess = new PolynomOperations();
                	labelActualResult.setText(operationsAccess.integr(polynoms));
                }
            }
        });
		
		}
		
	
}
