package mon;

public class Monom {
	private int exp;
	private int coef;

	public Monom(int coef, int exp) {
		if (exp != 0)
			this.exp = exp;
		if (coef != 0)
			this.coef = coef;
	}

	public int Exp() {
		return exp;
	}

	public int Coef() {
		return coef;
	}

	
}
