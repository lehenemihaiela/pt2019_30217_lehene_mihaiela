package main;

import javax.swing.SwingUtilities;

import view.View;

public class Main {
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				
				View window = new View();
                window.frame.setVisible(true);
				
			}
		});
	}
}